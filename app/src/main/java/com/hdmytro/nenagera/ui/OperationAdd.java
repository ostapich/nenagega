package com.hdmytro.nenagera.ui;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Operation;
import com.hdmytro.nenagera.persistence.helper.OperationTypeCollection;
import com.hdmytro.nenagera.persistence.view.OperationViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OperationAdd extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener  {

    private Spinner mOperationType;
    private EditText mOperationDesc;
    private TextView mOperationDate;
    private EditText mAmmount;
    private Operation mCurrentOperation;
    private OperationViewModel mOperationViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operation_add);
        mOperationType = findViewById(R.id.operation_type);
        mOperationDesc = findViewById(R.id.operation_description_field);
        mOperationDate = findViewById(R.id.operation_date);
        mAmmount = findViewById(R.id.operation_ammount);

        OperationTypeCollection operations = new OperationTypeCollection(getResources());
        ArrayAdapter<OperationTypeCollection.OperationType> spinnerAdapter = new ArrayAdapter<OperationTypeCollection.OperationType>(
                this,
                android.R.layout.simple_spinner_item,
                operations.getTypes()
        );

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mOperationType.setAdapter(spinnerAdapter);

        mOperationViewModel = ViewModelProviders.of(this).get(OperationViewModel.class);
        int id = getIntent().getIntExtra("OPERATION_ID", 0);
        if (id > 0) {
            this.setData(id);
        }
    }

    public void setDateField(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        TextView dateField = findViewById(R.id.operation_date);
        month++;
        dateField.setText(dayOfMonth + "/" + month + "/" + year);

    }

    public void saveOperation(View view) {
        String operationDescription = mOperationDesc.getText().toString();
        OperationTypeCollection.OperationType operationType = (OperationTypeCollection.OperationType) mOperationType.getSelectedItem();
        String strDate = (String) mOperationDate.getText();
        Double ammount = Double.valueOf(mAmmount.getText().toString());
        int walletId = getWalletId();
        int operationId = getOperationId();
        Operation  operation = new Operation(walletId, operationType.getType(), getDate(strDate), operationDescription, ammount);
        if (operationId != -1) {
           operation.setId(operationId);
        }

        mOperationViewModel.insert(operation);
        finish();
    }

    private void setData(int id) {
        mOperationViewModel.getOperationById(id).observe(this, new Observer<Operation>() {
            @Override
            public void onChanged(@Nullable Operation operation) {
                mCurrentOperation = operation;
                OperationTypeCollection operations = new OperationTypeCollection(getResources());
                mOperationType.setSelection(operations.getPositionByType(operation.getOperationType()));
                mOperationDesc.setText(operation.getOperationDescription());
                SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
                mOperationDate.setText(dateFormat.format(operation.getDate()));
                mAmmount.setText(Double.toString(operation.getAmount()));
            }
        });
    }

    private int getWalletId() {
        String[] walletInfo = getIntent().getStringArrayExtra("WALLET");
        int walletId = 0;
        if(walletInfo != null) {
            walletId = Integer.parseInt(walletInfo[0]);
        } else if (mCurrentOperation != null) {
            walletId = mCurrentOperation.getWalletId();
        }

        return walletId;
    }

    private int getOperationId() {
        if (mCurrentOperation != null) {
            return mCurrentOperation.getId();
        }
        return -1;
    }

    private Date getDate(String strDate) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
        try {
            date = dateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

}
