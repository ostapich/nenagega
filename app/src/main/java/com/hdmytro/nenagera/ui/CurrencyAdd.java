package com.hdmytro.nenagera.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Currency;
import com.hdmytro.nenagera.persistence.helper.CurrencyCodeCollection;
import com.hdmytro.nenagera.persistence.helper.OperationTypeCollection;
import com.hdmytro.nenagera.persistence.view.CurrencyViewModel;

public class CurrencyAdd extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.example.android.wordlistsql.REPLY";

    private EditText mNameField;
    private Spinner mCurrencySpinner;

    private CurrencyViewModel mCurrencyViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_add);
        mNameField = findViewById(R.id.currency_add_name_field);
        mCurrencySpinner = findViewById(R.id.curency_spinner);

        mCurrencyViewModel = ViewModelProviders.of(this).get(CurrencyViewModel.class);

        String[] currencyInfo = getIntent().getStringArrayExtra("CURRENCY");

        CurrencyCodeCollection curencyCodeList = new CurrencyCodeCollection(getResources());
        ArrayAdapter<CurrencyCodeCollection.CurrencyCode> spinnerAdapter = new ArrayAdapter<CurrencyCodeCollection.CurrencyCode>(
                this,
                android.R.layout.simple_spinner_item,
                curencyCodeList.getCodes()
        );

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCurrencySpinner.setAdapter(spinnerAdapter);

        if (currencyInfo != null) {
            mCurrencySpinner.setSelection(curencyCodeList.getPositionByCode(currencyInfo[0]));
            mNameField.setText(currencyInfo[1]);
        }

    }

    public void saveCurrency(android.view.View view) {
        CurrencyCodeCollection.CurrencyCode cuurencyCode =
                (CurrencyCodeCollection.CurrencyCode) mCurrencySpinner.getSelectedItem();

        String currencyName = mNameField.getText().toString();

        if (!currencyName.equals("")) {
            Currency newcurrency = new Currency(cuurencyCode.getCode(), currencyName);
            mCurrencyViewModel.insert(newcurrency);
        } else {
            Log.d("Nenagera", "shouldn't save");
        }

        finish();
    }
}
