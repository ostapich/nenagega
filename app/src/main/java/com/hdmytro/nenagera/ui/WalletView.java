package com.hdmytro.nenagera.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Operation;
import com.hdmytro.nenagera.persistence.helper.OperationTypeCollection;
import com.hdmytro.nenagera.persistence.recycler.OperationlistAdapter;
import com.hdmytro.nenagera.persistence.view.OperationViewModel;

import java.util.List;

public class WalletView extends NavActivity {

    private OperationViewModel mOperationView;
    private String[] walletInfo;
    private OperationlistAdapter adapter;
    private Operation clickedOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_view);

        setSelfClass(WalletView.this);
        setDrawerId(R.id.drawer_walletview_layout);
        initToolBar();
        setToolbarTitle("Wallet");

        RecyclerView recyclerView = findViewById(R.id.wallet_recyclerview);
        adapter = new OperationlistAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        walletInfo = getIntent().getStringArrayExtra("WALLET");

        adapter.setOnItemClickListener(new OperationlistAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Operation operation, int position) {
                Intent intent = new Intent(getApplicationContext(), OperationAdd.class);
                intent.putExtra("OPERATION_ID", operation.getId());
                startActivity(intent);
            }
        });

        adapter.setOnItemCreateContextMenu(new OperationlistAdapter.OnItemCreateContextMenuListener() {
            @Override
            public void onItemCreateContextMenu(Operation operation, int position, ContextMenu menu) {
                MenuInflater menuInflater = getMenuInflater();
                menuInflater.inflate(R.menu.currency_float_menu, menu);
                clickedOperation = operation;
            }
        });


        mOperationView = ViewModelProviders.of(this).get(OperationViewModel.class);
        mOperationView.getListByWallet(Integer.valueOf(walletInfo[0])).observe(this, new Observer<List<Operation>>() {
            @Override
            public void onChanged(@Nullable List<Operation> operations) {
                adapter.setmOperationList(operations);
                TextView currentAmount = findViewById(R.id.wallet_current_amount);
                currentAmount.setText("Summ: " + getAmount(operations));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mOperationView.getListByWallet(Integer.valueOf(walletInfo[0])).observe(this, new Observer<List<Operation>>() {
            @Override
            public void onChanged(@Nullable List<Operation> operations) {
                TextView currentAmount = findViewById(R.id.wallet_current_amount);
                currentAmount.setText("Summ: " + getAmount(operations));
            }
        });
    }

    public void addNewOperation(View view) {

        Intent addOperationIntent = new Intent(this, OperationAdd.class);
        addOperationIntent.putExtra("WALLET", walletInfo);
        startActivity(addOperationIntent);
    }

    private void filterOperationByType(String type) {
        if (type.equals("all")) {
            mOperationView.getListByWallet(Integer.valueOf(walletInfo[0])).observe(this, new Observer<List<Operation>>() {
                @Override
                public void onChanged(@Nullable List<Operation> operations) {
                    adapter.setmOperationList(operations);
                }
            });
        } else {
            mOperationView.getFilterListByTpe(Integer.valueOf(walletInfo[0]), type).observe(this, new Observer<List<Operation>>() {
                @Override
                public void onChanged(@Nullable List<Operation> operations) {
                    adapter.setmOperationList(operations);
                }
            });
        }
    }

    public void filterByOut(View view) {
        this.filterOperationByType(OperationTypeCollection.OUTCOME);
    }

    public void filterByInc(View view) {
        this.filterOperationByType(OperationTypeCollection.INCOMING);
    }

    public void filterByAll(View view) {
        this.filterOperationByType(OperationTypeCollection.ALL);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.currency_float_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.currency_fl_delete :
                deleteOperation();
                break;
        }
        return true;
    }

    private void deleteOperation() {
        if (clickedOperation != null) {
            mOperationView.deleteOperation(clickedOperation);
        }
    }

    private double getAmount(List<Operation> operations) {
        double amount = 0;
        for (Operation currOperation : operations) {
            if (currOperation.getOperationType().equals(OperationTypeCollection.INCOMING)) {
                amount += currOperation.getAmount();
            } else if (currOperation.getOperationType().equals(OperationTypeCollection.OUTCOME)) {
                amount -= currOperation.getAmount();
            }
        }

        return amount;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.walletview_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if (id == R.id.walletview_toolbar_chart) {
            Toast.makeText(WalletView.this, "Action clicked", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
