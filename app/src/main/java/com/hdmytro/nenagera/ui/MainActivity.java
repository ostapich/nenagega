package com.hdmytro.nenagera.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Wallet;
import com.hdmytro.nenagera.persistence.recycler.WalletlistAdapter;
import com.hdmytro.nenagera.persistence.view.WalletViewModel;

import java.util.List;

public class MainActivity extends NavActivity {

    private WalletViewModel mWalletViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSelfClass(MainActivity.this);
        setDrawerId(R.id.drawer_layout);
        initToolBar();

        RecyclerView recyclerView = findViewById(R.id.wallet_recyclerview);
        final WalletlistAdapter adapter = new WalletlistAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mWalletViewModel = ViewModelProviders.of(this).get(WalletViewModel.class);
        mWalletViewModel.getmWalletList().observe(this, new Observer<List<Wallet>>() {
            @Override
            public void onChanged(@Nullable List<Wallet> wallets) {
                adapter.setmWalletList(wallets);
            }
        });

        adapter.setOnItemClickListener(new WalletlistAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Wallet wallet, int position) {
                Intent intent = new Intent(getApplicationContext(), WalletView.class);
                String[] walletData = {String.valueOf(wallet.getId()), wallet.getName()};
                intent.putExtra("WALLET", walletData);
                startActivity(intent);
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("Nenagera", "main activity list");
    }

    public void activateWalletAdd(View view) {

        Intent addWaletIntent = new Intent(MainActivity.this, WalletAdd.class);
        startActivity(addWaletIntent);

    }

    public void viewWallet(View view) {
//        view.get
//        int position = view.getOnClickListener();
//        Log.d("Nenagera",  Integer.toString(position));
    }

}
