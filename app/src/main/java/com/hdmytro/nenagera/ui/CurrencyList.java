package com.hdmytro.nenagera.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Currency;
import com.hdmytro.nenagera.persistence.recycler.CurrencylistAdapter;
import com.hdmytro.nenagera.persistence.view.CurrencyViewModel;

import java.util.List;

public class CurrencyList extends NavActivity {

    private CurrencyViewModel mCurrencyViewModel;

    private Currency currentClickedCurrency;

    private RecyclerView recyclerView;

    public static final int NEW_CURRENCY_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_list);

        setSelfClass(CurrencyList.this);
        setDrawerId(R.id.drawer_currency_list);
        initToolBar();
        recyclerView = findViewById(R.id.recyclerview);
        final CurrencylistAdapter adapter = new CurrencylistAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter.setOnItemClickListener(new CurrencylistAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Currency currency, int position) {
                Intent intent = new Intent(getApplicationContext(), CurrencyAdd.class);
                String[] walletData = {currency.getCode(), currency.getName()};
                intent.putExtra("CURRENCY", walletData);
                startActivity(intent);
            }
        });

        adapter.setOnItemCreateContextMenu(new CurrencylistAdapter.OnItemCreateContextMenuListener() {
            @Override
            public void onItemCreateContextMenu(Currency currency, int position, ContextMenu menu) {
                MenuInflater menuInflater = getMenuInflater();
                menuInflater.inflate(R.menu.currency_float_menu, menu);
                currentClickedCurrency = currency;
            }
        });


        mCurrencyViewModel = ViewModelProviders.of(this).get(CurrencyViewModel.class);

        mCurrencyViewModel.getCurrencyList().observe(this, new Observer<List<Currency>>() {
            @Override
            public void onChanged(@Nullable List<Currency> currencies) {
                adapter.setCurrencies(currencies);
            }
        });
    }

    public void activateCurrencyAdd(View view) {
        Intent currencyAdd = new Intent(CurrencyList.this, CurrencyAdd.class);
        startActivity(currencyAdd);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.currency_float_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.currency_fl_delete :
                deleteCurrency();
                break;
        }
        return true;
    }

    private void deleteCurrency() {
        if (currentClickedCurrency != null) {
            mCurrencyViewModel.deleteCurrency(currentClickedCurrency.getCode());
        }
    }


}
