package com.hdmytro.nenagera.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Currency;
import com.hdmytro.nenagera.persistence.entity.Operation;
import com.hdmytro.nenagera.persistence.entity.Wallet;
import com.hdmytro.nenagera.persistence.helper.OperationTypeCollection;
import com.hdmytro.nenagera.persistence.view.CurrencyViewModel;
import com.hdmytro.nenagera.persistence.view.OperationViewModel;
import com.hdmytro.nenagera.persistence.view.WalletViewModel;

import java.util.Date;
import java.util.List;

public class WalletAdd extends AppCompatActivity {

    private EditText mWalletName;

    private EditText mWalletAmount;

    private Spinner mCurrencyCode;

    private CurrencyViewModel mCurrencyViewModel;

    private List<Currency> mcurrencies;

    private WalletViewModel mWalletViewModel;

    private OperationViewModel mOperationViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add);

        mWalletName = findViewById(R.id.wallet_add_name_field);
        mWalletAmount = findViewById(R.id.wallet_amount_field);
        mCurrencyCode = findViewById(R.id.wallet_add_currency_spiner);

        mOperationViewModel = ViewModelProviders.of(this).get(OperationViewModel.class);
        mCurrencyViewModel = ViewModelProviders.of(this).get(CurrencyViewModel.class);
        mCurrencyViewModel.getCurrencyList().observe(this, new Observer<List<Currency>>() {
            @Override
            public void onChanged(@Nullable List<Currency> currencies) {
                mcurrencies = currencies;
                populateCurrency();
            }
        });

        mWalletViewModel = ViewModelProviders.of(this).get(WalletViewModel.class);
    }

    public void saveWallet(View view) {
        String tmpWaletName = mWalletName.getText().toString();
        Double amount = Double.parseDouble(mWalletAmount.getText().toString());
        mCurrencyCode = (Spinner) findViewById(R.id.wallet_add_currency_spiner);
        Currency selectedCurrency = (Currency) mCurrencyCode.getSelectedItem();

        if (!tmpWaletName.equals("") && selectedCurrency != null) {
            Wallet tmpWallet = new Wallet(
                    tmpWaletName,
                    selectedCurrency.getCode()
            );
            Long walletId = mWalletViewModel.insert(tmpWallet);
            Operation tmpOperation = new Operation(
                    walletId.intValue(),
                    OperationTypeCollection.INCOMING,
                    new Date(),
                    "Initialization of wallet",
                    amount
            );
            mOperationViewModel.insert(tmpOperation);
            finish();
        }
    }

    private void populateCurrency() {
        if (mcurrencies != null) {

            ArrayAdapter<Currency> dataAdapter = new ArrayAdapter<Currency>(
                    this,
                    android.R.layout.simple_spinner_dropdown_item,
                    mcurrencies
            );
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCurrencyCode.setAdapter(dataAdapter);
        }
    }
}
