package com.hdmytro.nenagera.ui;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.hdmytro.nenagera.R;

public class NavActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private DrawerLayout mDrawerLayout;

    private AppCompatActivity selfClass;

    private int drawerId;

    protected Toolbar toolbar;

    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeAsUpIndicator(R.drawable.ic_mene);
        mDrawerLayout = findViewById(getDrawerId());
        NavigationView mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItemClick(menuItem);
                        return true;
                    }
                }
        );
    }

    private void menuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_changePhoto :
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
                break;
            case R.id.nav_curency_list :
                Intent listIntent = new Intent(getSelfClass(), CurrencyList.class);
                startActivity(listIntent);
                finish();
                break;
            case R.id.nav_home :
                Intent mainIntent = new Intent(getSelfClass(), MainActivity.class);
                startActivity(mainIntent);
                finish();
                break;
        }

        mDrawerLayout.closeDrawers();
    }

    protected void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public int getDrawerId() {
        return drawerId;
    }

    public void setDrawerId(int drawerId) {
        this.drawerId = drawerId;
    }

    public AppCompatActivity getSelfClass() {
        return selfClass;
    }

    public void setSelfClass(AppCompatActivity selfClass) {
        this.selfClass = selfClass;
    }
}
