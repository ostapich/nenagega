package com.hdmytro.nenagera.persistence.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.hdmytro.nenagera.persistence.NenageraRoomDatabase;
import com.hdmytro.nenagera.persistence.dao.WalletDao;
import com.hdmytro.nenagera.persistence.entity.Wallet;

import java.util.List;

public class WalletRepository {
    private WalletDao mWalletDao;
    private LiveData<List<Wallet>> mWalletList;

    public WalletRepository(Application app) {
        NenageraRoomDatabase db = NenageraRoomDatabase.getDatabase(app);
        mWalletDao = db.walletDao();
        mWalletList = mWalletDao.getAllWallets();
    }

    public LiveData<List<Wallet>> getmWalletList() {
        return mWalletList;
    }

    public Long insert(Wallet wallet) {
        Long id;
        try {
            id = new insertAsyncTask(mWalletDao).execute(wallet).get();
        } catch (Exception e) {
            id = new Long(-1);
        }

        return id;
    }

    private static class insertAsyncTask extends AsyncTask<Wallet, Void, Long> {
        private WalletDao mAsyncTaskDao;

        public insertAsyncTask(WalletDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Long doInBackground(final Wallet... wallets) {
            return mAsyncTaskDao.insert(wallets[0]);
        }
    }
}
