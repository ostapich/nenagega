package com.hdmytro.nenagera.persistence.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import static android.arch.persistence.room.ForeignKey.NO_ACTION;

@Entity(tableName = "wallet",
        foreignKeys = @ForeignKey(
                entity = com.hdmytro.nenagera.persistence.entity.Currency.class,
                parentColumns = "code",
                childColumns = "currency",
                onDelete = NO_ACTION))
public class Wallet {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "currency")
    private String currency;

    public Wallet(@NonNull String name, @NonNull String currency) {
        this.name = name;
        this.currency = currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(@NonNull String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return name;
    }
}
