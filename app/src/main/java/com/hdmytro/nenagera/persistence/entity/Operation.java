package com.hdmytro.nenagera.persistence.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import static android.arch.persistence.room.ForeignKey.NO_ACTION;

import java.util.Date;

@Entity(tableName = "operation",
         foreignKeys = @ForeignKey(
                 entity = com.hdmytro.nenagera.persistence.entity.Wallet.class,
                 parentColumns = "id",
                 childColumns = "wallet_id",
                 onDelete = NO_ACTION))
public class Operation {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "wallet_id")
    private int walletId;

    @NonNull
    @ColumnInfo(name = "operation_type")
    private String operationType;

    @NonNull
    @ColumnInfo(name = "amount")
    private double amount;

    @NonNull
    @ColumnInfo(name = "date")
    private Date date;

    @NonNull
    @ColumnInfo(name = "operation_description")
    private String operationDescription;

    public Operation(int walletId, @NonNull String operationType, Date date, String operationDescription, double amount) {
        this.walletId = walletId;
        this.operationType = operationType;
        this.date = date;
        this.operationDescription = operationDescription;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    @NonNull
    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(@NonNull String operationType) {
        this.operationType = operationType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @NonNull
    public String getOperationDescription() {
        return operationDescription;
    }

    public void setOperationDescription(@NonNull String operationDescription) {
        this.operationDescription = operationDescription;
    }
}
