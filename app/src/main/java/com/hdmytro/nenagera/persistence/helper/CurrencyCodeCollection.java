package com.hdmytro.nenagera.persistence.helper;

import android.content.res.Resources;

import java.util.ArrayList;
import java.util.List;
import com.hdmytro.nenagera.R;

public class CurrencyCodeCollection {
    List<CurrencyCode> codes;

    public CurrencyCodeCollection(Resources res) {
        codes = new ArrayList<>();
        String[] tempCodes = res.getStringArray(R.array.cuurency_code);

        for(int i = 0; i < tempCodes.length; i++) {
            String[] codesNames = tempCodes[i].split(",");
            codes.add(new CurrencyCode(codesNames[1], codesNames[0]));
        }
    }

    public List<CurrencyCode> getCodes() {
        return codes;
    }

    public int getPositionByCode(String code) {
        int position = -1;
        for(CurrencyCode curCode : codes) {
            if (curCode.getCode().equals(code)) {
                position = codes.indexOf(curCode);
            }
        }

        return position;
    }

    public class CurrencyCode {
        private String name;
        private String code;

        public CurrencyCode(String name, String code) {
            this.name = name;
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        @Override
        public String toString() {
            return getName();
        }
    }
}
