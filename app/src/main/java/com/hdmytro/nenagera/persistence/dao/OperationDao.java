package com.hdmytro.nenagera.persistence.dao;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.hdmytro.nenagera.persistence.entity.Operation;

import java.util.List;

@Dao
public interface OperationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Operation operation);

    @Query("DELETE FROM operation")
    public void deleteAll();

    @Query("SELECT * FROM operation ORDER BY date ASC")
    public LiveData<List<Operation>> getList();

    @Query("SELECT * FROM operation WHERE wallet_id = :id ORDER BY date ASC")
    public LiveData<List<Operation>> getListByWallet(int id);

    @Query("SELECT * FROM operation WHERE wallet_id = :id AND operation_type = :type ORDER BY date ASC")
    public LiveData<List<Operation>> getFilterListByTpe(int id, String type);

    @Query("DELETE FROM operation WHERE id = :id")
    public void deleteOpearation(int id);

    @Query("SELECT * FROM operation WHERE id = :id")
    public LiveData<Operation> getOperationById(int id);
}
