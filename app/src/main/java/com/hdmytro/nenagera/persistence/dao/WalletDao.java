package com.hdmytro.nenagera.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.hdmytro.nenagera.persistence.entity.Wallet;

import java.util.List;

@Dao
public interface WalletDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insert(Wallet wallet);

    @Query("DELETE FROM wallet WHERE id=:id")
    public void deleteById(int id);

    @Query("SELECT * FROM wallet ORDER BY id")
    public LiveData<List<Wallet>> getAllWallets();
}
