package com.hdmytro.nenagera.persistence.recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Operation;

import java.util.List;


public class OperationlistAdapter extends RecyclerView.Adapter<OperationlistAdapter.OperationViewHolder> {

    private final LayoutInflater mInFlater;
    private List<Operation> mOperationList;
    private OnItemClickListener onItemClickListener;
    private OnItemCreateContextMenuListener onItemCreateContextMenuListener;

    public OperationlistAdapter(Context context) {
        mInFlater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(@NonNull OperationViewHolder operationViewHolder, int position) {
        Operation currentOperation = mOperationList.get(position);
        operationViewHolder.operationItemView.setText(currentOperation.getOperationDescription() + currentOperation.getDate().toString());
        operationViewHolder.item = getItem(position);
    }

    @NonNull
    @Override
    public OperationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = mInFlater.inflate(R.layout.recyclerview_walletoperation_item, viewGroup, false);
        return new OperationViewHolder(itemView);
    }

    public void setmOperationList(List<Operation> mOperationList) {
        this.mOperationList = mOperationList;
        notifyDataSetChanged();
    }

    public Operation getItem(int position) {
        return mOperationList != null ? mOperationList.get(position) : null;
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (mOperationList != null) {
            size = mOperationList.size();
        }

        return size;
    }

    class OperationViewHolder extends RecyclerView.ViewHolder {
        private final TextView operationItemView;
        private Operation item;

        public OperationViewHolder(@NonNull View itemView) {
            super(itemView);
            operationItemView = itemView.findViewById(R.id.waletoperation_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION && onItemClickListener != null) {
                        onItemClickListener.onItemClick(getItem(position), position);
                    }
                }
            });

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION && onItemCreateContextMenuListener != null) {
                        onItemCreateContextMenuListener.onItemCreateContextMenu(getItem(position), position, menu);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Operation operation, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public interface OnItemCreateContextMenuListener {
        void onItemCreateContextMenu(Operation operation, int position, ContextMenu menu);
    }

    public void setOnItemCreateContextMenu(OnItemCreateContextMenuListener listener) {
        this.onItemCreateContextMenuListener = listener;
    }
}
