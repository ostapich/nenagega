package com.hdmytro.nenagera.persistence.view;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.hdmytro.nenagera.persistence.entity.Wallet;
import com.hdmytro.nenagera.persistence.repositories.WalletRepository;

import java.util.List;

public class WalletViewModel extends AndroidViewModel {

    private WalletRepository mWalletRepository;
    private LiveData<List<Wallet>> mWalletList;

    public WalletViewModel(@NonNull Application application) {
        super(application);
        mWalletRepository = new WalletRepository(application);
        mWalletList = mWalletRepository.getmWalletList();
    }

    public Long insert(Wallet wallet) {
        return mWalletRepository.insert(wallet);
    }

    public LiveData<List<Wallet>> getmWalletList() {
        return mWalletList;
    }
}
