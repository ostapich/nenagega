package com.hdmytro.nenagera.persistence.helper;

import android.content.res.Resources;

import com.hdmytro.nenagera.R;

import java.util.ArrayList;
import java.util.List;

public class OperationTypeCollection {
    private List<OperationType> types;
    public static final String INCOMING = "inc";
    public static final String OUTCOME = "out";
    public static final String ALL = "all";

    public OperationTypeCollection(Resources res) {
        types = new ArrayList<OperationType>();
        types.add(new OperationType("", "none"));
        types.add(new OperationType(res.getString(R.string.operation_type_income), INCOMING));
        types.add(new OperationType(res.getString(R.string.operation_type_outcome), OUTCOME));
    }

    public int getPositionByType(String type) {
        int position = 0;
        for(OperationType curType : types) {
            if (curType.getType().equals(type)) {
                position = types.indexOf(curType);
                break;
            }
        }

        return position;
    }

    public List<OperationType> getTypes() {
        return types;
    }

    public class OperationType {
        private String name;
        private String type;

        public OperationType(String name, String type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
