package com.hdmytro.nenagera.persistence.view;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.hdmytro.nenagera.persistence.entity.Currency;
import com.hdmytro.nenagera.persistence.repositories.CurrencyRepository;

import java.util.List;

public class CurrencyViewModel extends AndroidViewModel {

    private CurrencyRepository mCurrencyRepository;
    private LiveData<List<Currency>> mCurrencyList;

    public CurrencyViewModel(@NonNull Application application) {
        super(application);
        mCurrencyRepository = new CurrencyRepository(application);
        mCurrencyList = mCurrencyRepository.getList();
    }

    public LiveData<List<Currency>> getCurrencyList() {
        return mCurrencyList;
    }

    public void insert(Currency currency) {
        mCurrencyRepository.insert(currency);
    }

    public void deleteCurrency(String code) {
        mCurrencyRepository.deleteCurrency(code);
    }
}
