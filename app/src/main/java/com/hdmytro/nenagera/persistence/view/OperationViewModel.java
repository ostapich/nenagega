package com.hdmytro.nenagera.persistence.view;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.hdmytro.nenagera.persistence.entity.Operation;
import com.hdmytro.nenagera.persistence.repositories.OperationRepository;

import java.util.List;

public class OperationViewModel extends AndroidViewModel {

    private OperationRepository mOperationRepository;
    private LiveData<List<Operation>> mOperationList;

    public OperationViewModel(@NonNull Application app) {
        super(app);
        mOperationRepository = new OperationRepository(app);
        mOperationList = mOperationRepository.getList();
    }

    public LiveData<List<Operation>> getOperationList() {
        return mOperationList;
    }

    public LiveData<List<Operation>> getListByWallet(int id) {
        return mOperationRepository.getListByWallet(id);
    }

    public LiveData<Operation> getOperationById(int id) {
        return mOperationRepository.getOperationById(id);
    }

    public LiveData<List<Operation>> getFilterListByTpe(int id, String type)  {
        return mOperationRepository.getFilterListByTpe(id, type) ;
    }

    public void deleteOperation(Operation operation) {
        mOperationRepository.deleteOperation(operation);
    }

    public void insert(Operation operation) {
        mOperationRepository.insert(operation);
    }
}
