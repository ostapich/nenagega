package com.hdmytro.nenagera.persistence.dao;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.hdmytro.nenagera.persistence.entity.Currency;

import java.util.List;

@Dao
public interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Currency currency);

    @Query("DELETE FROM currency")
    public void deleteAll();

    @Query("DELETE FROM currency WHERE code=:code")
    public void deleteCurrency(String code);

    @Query("SELECT * FROM currency ORDER BY code ASC")
    public LiveData<List<Currency>> getList();
}
