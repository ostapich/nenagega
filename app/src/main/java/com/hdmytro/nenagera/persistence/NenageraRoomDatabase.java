package com.hdmytro.nenagera.persistence;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.hdmytro.nenagera.persistence.converters.DateConverter;
import com.hdmytro.nenagera.persistence.dao.CurrencyDao;
import com.hdmytro.nenagera.persistence.dao.OperationDao;
import com.hdmytro.nenagera.persistence.dao.WalletDao;
import com.hdmytro.nenagera.persistence.entity.Currency;
import com.hdmytro.nenagera.persistence.entity.Operation;
import com.hdmytro.nenagera.persistence.entity.Wallet;

@Database(entities = {Currency.class, Wallet.class, Operation.class}, version = 6)
@TypeConverters({DateConverter.class})
public abstract class NenageraRoomDatabase extends RoomDatabase {

    public static volatile NenageraRoomDatabase INSTANCE;

    public abstract CurrencyDao currencyDao();

    public abstract WalletDao walletDao();

    public abstract OperationDao operationDao();

    public static NenageraRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (NenageraRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room
                            .databaseBuilder(
                                context.getApplicationContext(),
                                NenageraRoomDatabase.class,
                                "nenagera.db"
                            )
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final CurrencyDao currencyDao;

        public PopulateDbAsync(NenageraRoomDatabase db) {
            this.currencyDao = db.currencyDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            currencyDao.deleteAll();
            return null;
        }
    }
}
