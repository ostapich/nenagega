package com.hdmytro.nenagera.persistence.recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Currency;

import java.util.List;


public class CurrencylistAdapter extends RecyclerView.Adapter<CurrencylistAdapter.CurrencyViewHolder> {


    private final LayoutInflater mInFlater;
    private List<Currency> mCurrency;
    private OnItemClickListener listener;
    private OnItemCreateContextMenuListener onCreateContextMenuListener;

    public CurrencylistAdapter(Context context) {
        mInFlater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInFlater.inflate(R.layout.recyclerview_currency_item, parent, false);
        return new CurrencyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder currencyHolder, int position) {
        Currency current = mCurrency.get(position);
        currencyHolder.currencyItemView.setText(current.getName());
    }

    public void setCurrencies(List<Currency> currencies) {
        mCurrency = currencies;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mCurrency != null) {
            return mCurrency.size();
        } else {
            return 0;
        }

    }

    public Currency getItem(int position) {
        return mCurrency != null ? mCurrency.get(position) : null;
    }

    class CurrencyViewHolder extends RecyclerView.ViewHolder {
        private final TextView currencyItemView;

        public CurrencyViewHolder(@NonNull View itemView) {
            super(itemView);
            currencyItemView = itemView.findViewById(R.id.currency_item);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getItem(position), position);
                    }
                }
            });

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener(){
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        onCreateContextMenuListener.onItemCreateContextMenu(getItem(position), position, menu);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Currency currency, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public interface OnItemCreateContextMenuListener {
        void onItemCreateContextMenu(Currency currency, int position, ContextMenu menu);
    }

    public void setOnItemCreateContextMenu(OnItemCreateContextMenuListener onCreateContextMenuListener) {
        this.onCreateContextMenuListener = onCreateContextMenuListener;
    }
}
