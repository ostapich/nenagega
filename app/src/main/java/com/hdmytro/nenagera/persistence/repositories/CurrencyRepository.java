package com.hdmytro.nenagera.persistence.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.hdmytro.nenagera.persistence.NenageraRoomDatabase;
import com.hdmytro.nenagera.persistence.dao.CurrencyDao;
import com.hdmytro.nenagera.persistence.entity.Currency;

import java.util.List;

public class CurrencyRepository {
    private CurrencyDao mCurrencyDao;
    private LiveData<List<Currency>> mCurrencyList;

    public CurrencyRepository(Application app) {
        NenageraRoomDatabase db = NenageraRoomDatabase.getDatabase(app);
        mCurrencyDao = db.currencyDao();
        mCurrencyList = mCurrencyDao.getList();
    }

    public LiveData<List<Currency>> getList() {
        return mCurrencyList;
    }

    public void deleteCurrency(String code) {
        new deleteAsyncTask(mCurrencyDao).execute(code);
    }

    public void insert (Currency currency) {
        new insertAsyncTask(mCurrencyDao).execute(currency);
    }

    private static class insertAsyncTask extends AsyncTask<Currency, Void, Void> {

        private CurrencyDao mAsyncTaskDao;

        public insertAsyncTask(CurrencyDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(final Currency... currencies) {
            mAsyncTaskDao.insert(currencies[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<String, Void, Void> {
        private CurrencyDao mAsyncTaskDao;

        public deleteAsyncTask(CurrencyDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(String... currencies) {
            mAsyncTaskDao.deleteCurrency(currencies[0]);
            return null;
        }
    }
}
