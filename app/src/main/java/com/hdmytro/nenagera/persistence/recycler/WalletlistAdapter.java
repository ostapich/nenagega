package com.hdmytro.nenagera.persistence.recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hdmytro.nenagera.R;
import com.hdmytro.nenagera.persistence.entity.Wallet;

import java.util.List;


public class WalletlistAdapter extends RecyclerView.Adapter<WalletlistAdapter.WalletViewHolder> {

    private final LayoutInflater mInFlater;
    private List<Wallet> mWalletList;
    private OnItemClickListener listener;

    public WalletlistAdapter(Context context) {
        mInFlater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletViewHolder holder, int position) {
        Wallet currentWallet = mWalletList.get(position);
        holder.walletItemView.setText(currentWallet.getName());
        holder.item = mWalletList.get(position);
    }

    @NonNull
    @Override
    public WalletViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = mInFlater.inflate(R.layout.recyclerview_wallet_item, viewGroup, false);
        return new WalletViewHolder(itemView);
    }

    public Wallet getItem(int position) {
        return mWalletList != null ? mWalletList.get(position) : null;
    }

    public void setmWalletList(List<Wallet> mWalletList) {
        this.mWalletList = mWalletList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (mWalletList != null) {
            size = mWalletList.size();
        }

        return size;
    }

    class WalletViewHolder extends RecyclerView.ViewHolder {
        private final TextView walletItemView;
        private Wallet item;

        public WalletViewHolder(@NonNull View itemView) {
            super(itemView);
            walletItemView = itemView.findViewById(R.id.walet_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getItem(position), position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Wallet wallet, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
