package com.hdmytro.nenagera.persistence.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.hdmytro.nenagera.persistence.NenageraRoomDatabase;
import com.hdmytro.nenagera.persistence.dao.OperationDao;
import com.hdmytro.nenagera.persistence.entity.Operation;

import java.util.List;

public class OperationRepository {
    private OperationDao mOperationDao;
    private LiveData<List<Operation>> mOperationList;

    public OperationRepository(Application app) {
        NenageraRoomDatabase db = NenageraRoomDatabase.getDatabase(app);
        mOperationDao = db.operationDao();
        mOperationList = mOperationDao.getList();
    }

    public LiveData<List<Operation>> getList() {
        return mOperationList;
    }

    public LiveData<List<Operation>> getListByWallet(int id) {
        return mOperationDao.getListByWallet(id);
    }

    public LiveData<Operation> getOperationById(int id) {
        return mOperationDao.getOperationById(id);
    }

    public LiveData<List<Operation>>  getFilterListByTpe(int id, String type) {
        return mOperationDao.getFilterListByTpe(id, type);
    }

    public void insert (Operation operation) {
        new insertAsyncTask(mOperationDao).execute(operation);
    }

    public void deleteOperation(Operation operation) {
        new deleteAsyncTask(mOperationDao).execute(operation);
    }

    private static class insertAsyncTask extends AsyncTask<Operation, Void, Void> {

        private  OperationDao mAsyncTaskDao;

        public insertAsyncTask(OperationDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Operation... operations) {
            mAsyncTaskDao.insert(operations[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Operation, Void, Void> {

        private OperationDao mAsyncTaskDao;

        public deleteAsyncTask(OperationDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Operation... operations) {
            mAsyncTaskDao.deleteOpearation(operations[0].getId());
            return null;
        }
    }
}
